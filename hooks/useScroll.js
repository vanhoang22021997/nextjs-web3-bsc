import { useEffect } from 'react';

const useScroll = () => {
  const handleScroll = () => {
    const scrollTop = document.documentElement.scrollTop;
    const elmContainer = window.document.getElementById('info');
    const sidebar = window.document.getElementById('sidebar');

    if (!elmContainer && !sidebar) return;

    const elmScrollTop = elmContainer?.offsetTop;

    if (scrollTop > elmScrollTop) {
      console.log('sidebar');
      console.log(sidebar);
      sidebar.classList.add('fixSidebar');
    } else {
      sidebar.classList.remove('fixSidebar');
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.addEventListener('scroll', handleScroll);
    };
  }, []);
};

export default useScroll;
