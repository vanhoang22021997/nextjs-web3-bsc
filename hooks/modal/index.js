import { useDispatch, useSelector } from 'react-redux';

import {
  getWalletAccount,
  getWalletConnect,
  toggleWalletAccount,
  toggleWalletConnect,
} from 'store/modal/index';

export const useModalWalletConnect = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector(getWalletConnect);

  const toggleModal = () => {
    dispatch(toggleWalletConnect());
  };

  return { isOpen, onToggleConnectModal: toggleModal };
};

export const useModalWalletAccount = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector(getWalletAccount);

  const toggleModal = () => {
    dispatch(toggleWalletAccount());
  };

  return { isOpen, onToggleAccountModal: toggleModal };
};
