import { useEffect, useState } from 'react';
import BigNumber from 'bignumber.js';
import { useWeb3React } from '@web3-react/core';
import { getBep20Contract, getBulldogeContract } from 'helpers/contractHelpers';
import { BIG_ZERO } from 'helpers/bigNumber';
import useWeb3 from 'hooks/useWeb3';
import useRefresh from './useRefresh';

const useTokenBalance = (tokenAddress) => {
  const [balance, setBalance] = useState(BIG_ZERO);
  const {account} = useWeb3React();
  const web3 = useWeb3();
  const {fastRefresh} = useRefresh();

  useEffect(() => {
    const fetchBalance = async () => {
      const contract = getBep20Contract(tokenAddress, web3);
      const res = await contract.methods.balanceOf(account).call();
      setBalance(new BigNumber(res));
    };

    if (account) {
      fetchBalance();
    }
  }, [account, tokenAddress, web3, fastRefresh]);

  return balance;
};

export const useTotalSupply = () => {
  const {slowRefresh} = useRefresh();
  const [totalSupply, setTotalSupply] = useState(BIG_ZERO);

  useEffect(() => {
    async function fetchTotalSupply() {
      const bulldogeContract = getBulldogeContract();
      const supply = await bulldogeContract.methods.totalSupply().call();
      setTotalSupply(new BigNumber(supply));
    }

    fetchTotalSupply();
  }, [slowRefresh]);

  return totalSupply;
};

export const useTotalBurned = () => {
  const [balance, setBalance] = useState(BIG_ZERO);
  const {slowRefresh} = useRefresh();
  const web3 = useWeb3();

  useEffect(() => {
    const fetchBalance = async () => {
      const bulldogeContract = getBulldogeContract();
      const balance1 = await bulldogeContract.methods.balanceOf('0x56a94d936f8b0a94e817d13b6615a6d9a5b3b320').call();
      const balance2 = await bulldogeContract.methods.balanceOf('0xe71c62fc7197493b6e3634b861165bac548fca5e').call();
      setBalance(new BigNumber(balance1).plus(new BigNumber(balance2)));
    };

    fetchBalance();
  }, [web3, slowRefresh]);

  return balance;
};

export default useTokenBalance;
