const tokens = {
  bnb: {
    symbol: 'BNB',
    projectLink: 'https://www.binance.com/',
  },
  bulldoge: {
    symbol: 'BULLDOGE',
    address: {
      56: '0xe71c62fc7197493b6e3634b861165bac548fca5e',
    },
    decimals: 9,
    projectLink: 'https://bulldoge.dogen.finance/#/',
  },
}

export default tokens
