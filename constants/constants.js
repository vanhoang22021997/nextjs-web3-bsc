export const FOOTER_LINK = [
  {
    title: 'Social',
    children: [
      {
        title: 'Medium',
        link: '/',
      },
      {
        title: 'Telegram',
        link: '/',
      },
      {
        title: 'Twitter',
        link: '/',
      },
    ],
  },
  {
    title: 'Social',
    children: [
      {
        title: 'Medium',
        link: '/',
      },
      {
        title: 'Telegram',
        link: '/',
      },
      {
        title: 'Twitter',
        link: '/',
      },
    ],
  },
  {
    title: 'Company',
    children: [
      {
        title: 'Anout us',
        link: '/',
      },
      {
        title: 'Apply for IDO',
        link: '/',
      },
    ],
  },
  {
    title: 'Help',
    children: [
      {
        title: 'Support',
        link: '/',
      },
      {
        title: 'Terms',
        link: '/',
      },
      {
        title: 'Privacy',
        link: '/',
      },
    ],
  },
  {
    title: 'Developers',
    children: [
      {
        title: 'Documentation',
        link: '/',
      },
      {
        title: 'Onebite.js',
        link: '/',
      },
    ],
  },
];

export const ROWS_PER_PAGE = 10;
