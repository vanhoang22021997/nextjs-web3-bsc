export const CHAIN_ID = process.env.NEXT_PUBLIC_CHAIN_ID;
export const NODE_1 = process.env.NEXT_PUBLIC_NODE_1;
export const NODE_2 = process.env.NEXT_PUBLIC_NODE_2;
export const NODE_3 = process.env.NEXT_PUBLIC_NODE_3;
