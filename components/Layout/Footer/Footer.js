import Link from 'next/link';
import React from 'react';
import classnames from 'classnames';
import { Navbar } from 'reactstrap';
import Image from 'next/image';

import { useSkin } from 'hooks/useSkin';
import { FOOTER_LINK } from 'constants/constants';

import styles from 'styles/Layout.module.scss';

const Footer = () => {
  const [skin, setSkin] = useSkin();
  const year = new Date().getFullYear();

  return (
    <footer>
      <Navbar
        expand="lg"
        light={skin !== 'dark'}
        dark={skin === 'dark'}
        // color={bgColorCondition ? navbarColor : undefined}
        className={classnames(
          'header-navbar navbar align-items-center navbar-shadow',
          styles.footer,
        )}
      >
        <div className="container-xl">
          <div className="d-flex flex-wrap">
            <div className="mr-5">
              <Link href="/">
                <Image
                  className={styles.logo}
                  width={200}
                  height={50}
                  src="/images/onebit-logo.svg"
                />
              </Link>
              <p className={styles.copyright}>© Onebit {year}</p>
            </div>
            <div className={styles.footerLink}>
              {FOOTER_LINK.map((item, index) => (
                <div key={index}>
                  <p>{item.title}</p>
                  <div className="mt-2">
                    {item.children.map((child) => (
                      <a
                        key={child.title}
                        className="d-block"
                        href={child.link}
                      >
                        {child.title}
                      </a>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Navbar>
    </footer>
  );
};

export default Footer;
