import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useRouter } from 'next/router';

import Header from './Header/Header';
// import Footer from './Footer/Footer';

import styles from 'styles/Layout.module.scss';

const Layout = ({ children }) => {
  const { pathname } = useRouter();

  return (
    <div className={styles.container}>
      <Header />
      <div className={classnames('container-xl py-1', styles.content)}>
        {children}
      </div>
      {/*<Footer />*/}
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Layout;
