import React from 'react';
import { Navbar } from 'reactstrap';

import { useWeb3React } from '@web3-react/core';
import { formatAddress } from 'helpers/addressHelpers';

import Menu from 'components/Layout/Header/Menu';
import ModalWallet from 'components/Modal/ModalWallet';
import ButtonCustom from 'components/ButtonCustom/ButtonCustom';
import ModalAccount from 'components/Modal/ModalAccount';

import {
  useModalWalletAccount,
  useModalWalletConnect
} from 'hooks/modal/index';

import styles from 'styles/Layout.module.scss';

const Header = () => {
  const {account} = useWeb3React();
  const {onToggleConnectModal} = useModalWalletConnect();
  const {onToggleAccountModal} = useModalWalletAccount();

  return (
      <>
        <header>
          <Navbar
              className={styles.header}
          >
            <div className="container-xl">
              <a href="https://dogen.finance/#/">
                <img
                    className={styles.logo}
                    height={32}
                    src="/images/logo.png"
                />
                <span className={styles.logoText}>MasterDog</span>
              </a>
              <Menu/>
              <div>
                <ButtonCustom
                    size="sm"
                    onClick={account ? onToggleAccountModal : onToggleConnectModal}
                >
                  {account ? formatAddress(account) : 'Unlock Wallet'}
                </ButtonCustom>
              </div>
            </div>
          </Navbar>
        </header>
        <ModalWallet/>
        <ModalAccount/>
      </>
  );
};

export default Header;
