import React from 'react';

import ActiveLink from 'components/ActiveLink/ActiveLink';
import styles from 'styles/Layout.module.scss';

const Menu = () => {
  const menus = [
    {
      title: 'Home',
      path: 'https://dogen.finance/#/'
    },
    {
      title: 'Parks',
      path: 'https://dogen.finance/#/farms'
    },
    {
      title: 'Vote',
      path: 'https://snapshot.org/#/dogen.eth'
    }
  ];

  return (
      <div className="d-flex align-items-center justify-content-center flex-wrap">
        {menus.map((menu, index) => {
          const Link = menu.route ? ActiveLink : 'a';

          return (
              <Link
                  key={index}
                  activeclassname={styles.menuActive}
                  href={menu?.route || menu.path || '/'}
              >
                <p className={styles.menuItem}>{menu.title}</p>
              </Link>
          );
        })}
      </div>
  );
};

export default Menu;
