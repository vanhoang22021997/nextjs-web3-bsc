import React, { useState } from 'react';
import { Copy, Navigation } from 'react-feather';
import { useWeb3React } from '@web3-react/core';
import {
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  UncontrolledTooltip,
} from 'reactstrap';

import { useModalWalletAccount } from 'hooks/modal/index';
import useAuth from 'hooks/useAuth';

const ModalAccount = () => {
  const { logout } = useAuth();
  const { account } = useWeb3React();
  const { isOpen, onToggleAccountModal } = useModalWalletAccount();
  const [isCopy, setIsCopy] = useState(false);

  const handleLogout = () => {
    logout();
    onToggleAccountModal();
  };

  const copyToClipboard = () => {
    setIsCopy(true);
    navigator.clipboard.writeText(account);
  };

  return (
    <Modal
      isOpen={isOpen}
      toggle={onToggleAccountModal}
      className="modal-dialog-centered"
      size="sm"
    >
      <ModalHeader toggle={onToggleAccountModal}>
        <span className="text-normal-2 bold">Your wallet</span>
      </ModalHeader>
      <ModalBody className="text-center">
        <div>
          <p className="text-normal-1">{account}</p>
          <div className="d-flex justify-content-center align-items-center">
            <a href={`https://bscscan.com/address/${account}`} target="_blank">
              View on BscScan
            </a>
            <p
              onClick={copyToClipboard}
              id="copy"
              className="mb-0 ml-2 cursor-pointer hover-primary"
            >
              Copy Address <Copy />
            </p>
            <UncontrolledTooltip target="copy">
              {isCopy ? 'Copied' : 'copy'}
            </UncontrolledTooltip>
          </div>
        </div>
        <Button
          onClick={handleLogout}
          color="primary"
          className="round mt-2"
          outline
        >
          Logout
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default ModalAccount;
