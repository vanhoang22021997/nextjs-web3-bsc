import React from 'react';
import { Button, Modal, ModalBody, ModalHeader } from 'reactstrap';

import { useModalWalletConnect } from 'hooks/modal/index';
import useAuth from 'hooks/useAuth';

const ModalWallet = () => {
  const { login } = useAuth();
  const { isOpen, onToggleConnectModal } = useModalWalletConnect();

  const handleConnect = () => {
    login();
    onToggleConnectModal();
  };

  return (
    <Modal
      isOpen={isOpen}
      toggle={onToggleConnectModal}
      className="modal-dialog-centered"
      size="sm"
    >
      <ModalHeader toggle={onToggleConnectModal}>
        <span className="text-normal-2 bold">Connect to a wallet</span>
      </ModalHeader>
      <ModalBody className="text-center">
        <Button
          onClick={handleConnect}
          color="primary"
          className="round"
          outline
        >
          Metamask
          <img
            className="ml-2"
            src="/wallets/metamask-fox.svg"
            height="24"
            alt="Metamask"
          />
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default ModalWallet;
