import React, { useMemo } from 'react';

import styles from 'styles/Button.module.scss'

const ButtonCustom = ({
                        children,
                        disabled,
                        href,
                        onClick,
                        size,
                        text,
                        to,
                        variant,
                      }) => {
  let buttonColor
  let boxShadow
  let buttonSize
  let buttonPadding
  let fontSize

  switch (variant) {
    case 'secondary':
      buttonColor = "rgb(128, 94, 73)"
      break
    case 'default':
    default:
      buttonColor = "#d16c00"
  }

  switch (size) {
    case 'sm':
      buttonPadding = 16
      buttonSize = 36
      fontSize = 14
      break
    case 'md':
    default:
      buttonPadding = 24
      buttonSize = 56
      fontSize = 16
  }

  return (
      <button
          className={styles.button}
          style={{
            color: buttonColor,
            paddingLeft: buttonPadding,
            paddingRight: buttonPadding,
            fontSize: fontSize,
            height: buttonSize
          }}
          onClick={onClick}
      >
        {children}
        {text}
      </button>
  )
};

export default ButtonCustom;