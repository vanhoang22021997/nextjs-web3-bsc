import React from 'react';

const ChartTooltip = ({ active, payload }) => {
  if (active) {
    return (
      <div className="recharts-custom-tooltip">
        <span>{`
        ${payload[0].value}%`}</span>
      </div>
    );
  }

  return null;
};

export default ChartTooltip;
