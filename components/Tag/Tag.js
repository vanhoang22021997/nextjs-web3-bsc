import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Tag = ({ name, outline, className }) => {
  return (
    <>
      <div className={classnames('tag', className)}>{name}</div>
      <style jsx>{`
        .tag {
          color: ${outline ? 'var(--color-primary)' : '#ffffff'};
          background-color: ${outline ? '#212335' : 'var(--color-primary)'};
          min-width: 100px;
          min-height: 40px;
          border-radius: 50px;
          font-size: 18px;
          display: flex;
          align-items: center;
          justify-content: center;
        }
      `}</style>
    </>
  );
};

Tag.defaultProps = {
  outline: false,
};

Tag.propTypes = {
  name: PropTypes.string.isRequired,
  outline: PropTypes.bool,
};

export default Tag;
