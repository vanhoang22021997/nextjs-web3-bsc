import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import { Search } from 'react-feather';

const InputSearch = ({ onSearch }) => {
  const [search, setSearch] = useState('');

  const handleEnterSearch = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      onSearch();
    }
  };

  const handleSearchClick = async () => {
    onSearch();
  };

  return (
    <>
      <style jsx>{`
        .icon {
          border-right: none;
        }
      `}</style>
      <InputGroup className="round mx-0 py-0 mt-0">
        <InputGroupAddon
          className="icon"
          addonType="prepend"
          onClick={handleSearchClick}
        >
          <InputGroupText className="icon">
            <Search size={14} />
          </InputGroupText>
        </InputGroupAddon>
        <Input
          style={{
            borderLeft: 'none',
            maxWidth: 300,
          }}
          onChange={(e) => setSearch(e.target.value)}
          placeholder="Search..."
          value={search}
          onKeyPress={handleEnterSearch}
        />
      </InputGroup>
    </>
  );
};

InputSearch.propTypes = {
  onSearch: PropTypes.func.isRequired,
  setSearch: PropTypes.func.isRequired,
};

export default InputSearch;
