import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useCountUp } from 'react-countup'

const Value = ({
                 value,
                 decimals,
                 fontSize = '40px',
                 lineHeight = '1',
                 prefix = '',
                 bold = true,
                 color = 'text',
               }) => {
  const { countUp, update } = useCountUp({
    start: 0,
    end: value,
    duration: 1,
    separator: ',',
    decimals:
    // eslint-disable-next-line no-nested-ternary
        decimals !== undefined ? decimals : value < 0 ? 4 : value > 1e5 ? 0 : 3,
  })

  const updateValue = useRef(update)

  useEffect(() => {
    updateValue.current(value)
  }, [value, updateValue])

  return (
      <p style={{
        fontSize,
        lineHeight,
        color
      }}
      className={classnames(bold && 'font-weight-bold')}
      >
        {prefix}
        {countUp}
      </p>
  );
};

Value.propTypes = {
  value: PropTypes.string.isRequired,
  decimals: PropTypes.number,
  fontSize: PropTypes.string,
  lineHeight: PropTypes.string,
  prefix: PropTypes.string,
  bold: PropTypes.bool,
  color: PropTypes.string,
};

export default Value;