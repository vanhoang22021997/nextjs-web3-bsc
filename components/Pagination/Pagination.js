import { ROWS_PER_PAGE } from 'constants/constants';
import React from 'react';
import ReactPaginate from 'react-paginate';

const Pagination = ({ currentPage, handlePagination, total }) => {
  const count = Number((total / ROWS_PER_PAGE).toFixed(0));

  return (
    <ReactPaginate
      previousLabel={''}
      nextLabel={''}
      breakLabel="..."
      pageCount={count || 1}
      marginPagesDisplayed={2}
      pageRangeDisplayed={2}
      activeClassName="active"
      initialPage={1}
      // forcePage={currentPage !== 0 ? currentPage - 1 : 0}
      // onPageChange={page => handlePagination(page)}
      pageClassName={'page-item'}
      nextLinkClassName={'page-link'}
      nextClassName={'page-item next'}
      previousClassName={'page-item prev'}
      previousLinkClassName={'page-link'}
      pageLinkClassName={'page-link'}
      breakClassName="page-item"
      breakLinkClassName="page-link"
      containerClassName={
        'pagination react-paginate separated-pagination pagination-sm justify-content-end'
      }
    />
  );
};

export default Pagination;
