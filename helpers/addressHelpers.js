import { CHAIN_ID } from 'config/index';
import addresses from 'constants/contracts'
import tokens from 'constants/tokens'

export const getAddress = (address) => {
  const mainNetChainId = 56
  const chainId = CHAIN_ID
  return address[chainId] ? address[chainId] : address[mainNetChainId]
}

export const getBulldogeAddress = () => {
  return getAddress(tokens.bulldoge.address)
}

export const getClaimAddress = () => {
  return getAddress(addresses.claim)
}

export const formatAddress = (address) =>
    `${address.substring(0, 4)}...${address.substring(address.length - 4)}`;
