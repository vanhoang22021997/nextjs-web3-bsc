import { InjectedConnector } from '@web3-react/injected-connector';
import { CHAIN_ID } from 'config/index';

const chainId = parseInt(CHAIN_ID, 10);

export const injected = new InjectedConnector({ supportedChainIds: [chainId] });

export const getLibrary = (provider) => {
  return provider;
};
