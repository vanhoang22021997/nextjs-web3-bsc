// Array of available nodes to connect to
import { NODE_1, NODE_2, NODE_3 } from 'config/index';

export const nodes = [NODE_1, NODE_2, NODE_3];

const getNodeUrl = () => {
  const randomIndex = Math.floor(Math.random() * nodes.length);
  return nodes[randomIndex];
};

export default getNodeUrl;
