import web3NoAccount from 'helpers/web3'

// Addresses
import {
    getBulldogeAddress,
    getClaimAddress
} from 'helpers/addressHelpers'

// ABI
import claimABI from 'config/abi/claim.json'
import bep20Abi from 'config/abi/erc20.json'
import bulldogeAbi from 'config/abi/bulldogen.json'

const getContract = (abi, address, web3) => {
  const _web3 = web3 ?? web3NoAccount
  return new _web3.eth.Contract(abi, address)
}

export const getBep20Contract = (address, web3) => {
  return getContract(bep20Abi, address, web3)
}

export const getClaimContract = (address, web3) => {
  return getContract(claimABI, getClaimAddress(), web3)
}

export const getBulldogeContract = (address, web3) => {
  return getContract(bulldogeAbi, getBulldogeAddress(), web3)
}