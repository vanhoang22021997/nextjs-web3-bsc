import ClaimModal from 'modules/Home/components/ClaimModal/ClaimModal';
import React, { useState } from 'react';

import BulldogeStats from 'modules/Home/components/BulldogeStats/BulldogeStats';
import ButtonCustom from 'components/ButtonCustom/ButtonCustom';

import styles from 'styles/Home.module.scss'

export default function Home() {
  const [showClaim, setShowClaim] = useState(false);

  return (
    <div>
      <BulldogeStats/>
      <a
          className="read-more"
          target="_blank"
          href="https://dogenfinance.medium.com/codename-bulldoge-f313128893b3">
        Read more about BULLDOGE
      </a>
      <div className={styles.banner}/>
      <div className={styles.containerClaim}>
        <ButtonCustom
            text="ClaimModal"
            onClick={() => setShowClaim(true)}
            variant="secondary"
        />
        <div style={{
          marginTop: 10
        }}/>
        <ButtonCustom
            text="Trade"
            onClick={() => {
              window.open('https://exchange.pancakeswap.finance/#/swap?inputCurrency=0xe71c62fc7197493b6e3634b861165bac548fca5e', "_blank")
            }}
            variant="secondary"
        />
      </div>
      <ClaimModal show={showClaim} onClose={() => setShowClaim(false)}/>
    </div>
  );
}
