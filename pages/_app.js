import { Web3ReactProvider } from '@web3-react/core';
import Head from 'next/head';
import { Provider } from 'react-redux';
import BigNumber from 'bignumber.js'

import { getLibrary } from 'helpers/web3React';

import 'styles/globals.scss';

import Layout from 'components/Layout/Layout';

import { RefreshContextProvider } from 'context/RefreshContext';

import store from 'store';

// This config is required for number formating
BigNumber.config({
  EXPONENTIAL_AT: 1000,
  DECIMAL_PLACES: 80,
})

function App({Component, pageProps}) {
  return (
      <Web3ReactProvider getLibrary={getLibrary}>
        <Head>
          <title>Dogen Finance</title>
          <meta charSet="utf-8"/>
          <meta
              name="viewport"
              content="initial-scale=1.0, width=device-width"/>
          <link
              rel="icon"
              type="image/x-icon"
              href="/favicon/favicon.png"/>
          <link
              href="https://fonts.googleapis.com/css2?family=Kaushan+Script&family=Noto+Sans:wght@400;700&family=Reem+Kufi&family=Roboto+Mono:wght@400;700&display=swap"
              rel="stylesheet"
          />
        </Head>
        <Provider store={store}>
          <RefreshContextProvider>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </RefreshContextProvider>
        </Provider>
      </Web3ReactProvider>
  );
}

export default App;
