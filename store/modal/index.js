import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  walletConnect: false,
  walletAccount: false,
};

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    toggleWalletConnect: (state, action) => {
      state.walletConnect = !state.walletConnect;
    },
    toggleWalletAccount: (state, action) => {
      state.walletAccount = !state.walletAccount;
    },
  },
});

export const getWalletConnect = (state) => state.modal.walletConnect;
export const getWalletAccount = (state) => state.modal.walletAccount;

export const { toggleWalletConnect, toggleWalletAccount } = modalSlice.actions;

export default modalSlice.reducer;
