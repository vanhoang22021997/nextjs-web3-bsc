import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  user: {},
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    getUser: (state, action) => {
      state.user = action.payload;
    },
    logout: (state) => initialState,
  },
});

// async action

export const getUserLogged = (state) => state.auth.user;

// Actions
export const { getUser } = authSlice.actions;
export default authSlice.reducer;
