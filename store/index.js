import { configureStore } from '@reduxjs/toolkit';

import authReducer from './auth';
import modalReducer from './modal';

export default configureStore({
  reducer: {
    auth: authReducer,
    modal: modalReducer,
  },
  devTools: true,
});
