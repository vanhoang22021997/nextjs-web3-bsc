import React from 'react';

import { useTotalBurned, useTotalSupply } from 'hooks/useTokenBalance';
import Value from 'components/Value/Value';
import { getBalanceNumber } from 'helpers/formatBalance';

const BulldogeStats = () => {
  const totalSupply = useTotalSupply()
  const totalBurned = useTotalBurned()
  const remaining = totalSupply.minus(totalBurned)

  const percentTotalBurned = totalBurned.div(totalSupply).multipliedBy(100)
  const percentTotalRemaining = remaining.div(totalSupply).multipliedBy(100)

  return (
      <div>
        {
          totalSupply && <Value value={getBalanceNumber(totalSupply, 9)}/>
        }
        <Value decimals={0} value={getBalanceNumber(totalBurned, 9)}/>
        <Value decimals={0} value={getBalanceNumber(remaining, 9)}/>
      </div>
  );
};

export default BulldogeStats;