import { useWeb3React } from '@web3-react/core';
import ButtonCustom from 'components/ButtonCustom/ButtonCustom';
import { formatAddress } from 'helpers/addressHelpers';
import { useModalWalletConnect } from 'hooks/modal/index';
import React, { useState, useEffect } from 'react';
import { Button, Modal, FormFeedback, Label, Input, FormGroup, ModalFooter } from 'reactstrap';

import BigNumber from 'bignumber.js';
import useDebounce from 'hooks/useDebounce';
// import { claim } from '../../utils/web3';

import fileClaim from 'public/claim.json';

const ClaimModal = ({show, onClose}) => {
  const {account} = useWeb3React();
  const {onToggleConnectModal} = useModalWalletConnect();
  const [address, setAddress] = useState('');
  const debouncedSearchTerm = useDebounce(address, 500);
  const [data, setData] = useState({});
  const [error, setError] = useState('');

  const handleChange = (e) => {
    const {value} = e.target;
    setAddress(value);
  };

  const handleClaim = () => {
    claim(data.index, data.amount, data.proof);
  };

  const handleShowAmount = () => {
    const amount = new BigNumber(data.amount).div(1e9);
    return `${amount.toNumber().toLocaleString()} BULLDOGE`;
  };

  const handleClose = () => {
    setData({});
    setError('');
    setAddress('');
    onClose();
  };

  useEffect(() => {
    const checkAddress = () => {
      let data = undefined;
      for (const [key, value] of Object.entries(fileClaim)) {
        if (address === key) {
          data = value;
          break;
        }
      }

      if (data) {
        setData(data);
        setError('');
      } else {
        setData({});
        setError('Address has no available claim');
      }
    };
    if (debouncedSearchTerm) {
      checkAddress();
    }
  }, [debouncedSearchTerm]);

  return (
      <Modal
          onHide={onClose}
          isOpen={show}
          centered

      >
        <div style={{width: '100%'}}>
          <div style={{fontSize: 24, padding: 3, color: '#F5A623'}}>
            Claim BULLDOGE Token
          </div>
          <div
              style={{
                marginLeft: '3%',
                fontSize: 16,
                padding: 5
              }}
          >
            <div>
              <p>
                Enter an address to trigger a BULLDOGE claim. Please use only your wallet.
              </p>
              <FormGroup controlId="formBasicEmail">
                <Label>Recipient</Label>
                <Input
                    type="text"
                    placeholder="Wallet address"
                    onChange={handleChange}
                    value={address}
                    invalid={!!error}
                />
                <FormFeedback>{error}</FormFeedback>
              </FormGroup>
              {data.amount && <span>{handleShowAmount()}</span>}
            </div>
          </div>

          <ModalFooter>
            {
              account
                  ? <Button
                      active={data.amount}
                      className="btn-light btn-small mr-2 round"
                      size="sm"
                      onClick={handleClaim}
                      disabled={Object.keys(data).length === 0 || !account}
                  >
                    Claim BULLDOGE
                  </Button>
                  : <div>
                    <ButtonCustom
                        size="sm"
                        onClick={onToggleConnectModal}
                    >
                      Unlock Wallet
                    </ButtonCustom>
                  </div>
            }
            <Button className="btn-light btn-small round" size="sm" onClick={handleClose}>
              Close
            </Button>
          </ModalFooter>
        </div>
      </Modal>
  );
};

export default ClaimModal;
